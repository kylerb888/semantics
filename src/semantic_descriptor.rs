use dashmap::DashSet;
use std::borrow::Borrow;
use std::fmt::{Display, Write};
use std::hash::Hash;
use std::mem;
use std::str::FromStr;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;

use crate::semantic_dictionary::{FromStrError, SharedWord};
impl Hash for ContextWord {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.word.hash(state);
    }
}
impl Borrow<str> for ContextWord {
    fn borrow(&self) -> &str {
        &self.word
    }
}
impl PartialEq<ContextWord> for ContextWord {
    fn eq(&self, other: &Self) -> bool {
        self.word == other.word
    }
}
impl Display for ContextWord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} ", *self.word, self.count.load(Ordering::Relaxed))
    }
}
impl ContextWord {
    pub const fn new_with_count(ctx: SharedWord, count: u32) -> Self {
        Self {
            word: ctx,
            count: AtomicU32::new(count),
        }
    }
    pub const fn new(ctx: SharedWord) -> Self {
        Self {
            word: ctx,
            count: AtomicU32::new(1),
        }
    }
}
impl Eq for ContextWord {}
#[derive(Debug)]
pub struct ContextWord {
    word: SharedWord,
    count: AtomicU32,
}
impl Hash for SemanticDescriptor {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.target_word.hash(state);
    }
}
impl Eq for SemanticDescriptor {}
impl PartialEq for SemanticDescriptor {
    fn eq(&self, other: &Self) -> bool {
        self.target_word == other.target_word
    }
}
impl Borrow<str> for SemanticDescriptor {
    fn borrow(&self) -> &str {
        &self.target_word
    }
}
#[derive(Debug)]
pub struct SemanticDescriptor {
    target_word: SharedWord,
    context_words: DashSet<ContextWord, ahash::RandomState>,
}
impl SemanticDescriptor {
    pub fn from_str(s: &str, set: &DashSet<SharedWord, ahash::RandomState>) -> Result<Self, FromStrError> {
        let mut terms = s.split_whitespace();
        terms
            .next()
            .and_then(|target_word| {
                let brace = terms.next()?;
                assert!(brace == "{");
                let get = SemanticDescriptor::new(SharedWord::new(Arc::new(target_word.to_owned())));

                loop {
                    let context_word = terms.next()?;
                    if context_word == "}" {
                        break Some(get);
                    }
                    let count = terms.next()?.parse::<u32>().ok()?;
                    let shared = set.get(context_word).map_or_else(|| {
                        let new = SharedWord::new(Arc::new(context_word.to_owned()));
                        set.insert(new.clone());
                        new
                    }, |found| found.clone());
                    get.context_words.insert(ContextWord::new_with_count(
                        shared,
                        count,
                    ));
                }
            })
            .ok_or(FromStrError)
    }
    pub fn new(target_word: SharedWord) -> Self {
        Self {
            target_word,
            context_words: DashSet::default(),
        }
    }
    pub fn get_target_word(&self) -> &str {
        &self.target_word
    }
    pub fn process_context_word(&self, ctx: &SharedWord) {
        if let Some(found) = self.context_words.get(&****ctx) {
            found.count.fetch_add(1, Ordering::Relaxed);
        } else {
            self.context_words.insert(ContextWord::new(ctx.clone()));
        }
    }
}

impl Display for SemanticDescriptor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {{ ", *self.target_word)?;
        for context_word in self.context_words.iter() {
            write!(
                f,
                "{} {} ",
                *context_word.word,
                context_word.count.load(Ordering::Relaxed)
            )?
        }
        f.write_char('}')
    }
}

impl std::ops::Mul for &SemanticDescriptor {
    type Output = u32;

    fn mul(self, rhs: Self) -> Self::Output {
        self.context_words
            .iter()
            .filter_map(|i| {
                let rhs = rhs.context_words.get(i.key())?;

                Some(i.count.load(Ordering::Relaxed) * rhs.count.load(Ordering::Relaxed))
            })
            .sum()
    }
}
