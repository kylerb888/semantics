use std::{
    borrow::{Borrow, Cow},
    collections::HashSet,
    fmt::Display,
    fs::{self, File},
    io::{self, Read, Write, BufWriter, BufReader, BufRead},
    str::{FromStr, SplitWhitespace},
    sync::{Arc, Mutex, RwLock},
    time::Instant, path::Path,
};

use dashmap::DashSet;
use rayon::iter::{IntoParallelIterator, ParallelBridge, ParallelIterator, IntoParallelRefIterator};

use crate::{semantic_descriptor::SemanticDescriptor, DB_PATH};

#[derive(Debug)]
pub struct SemanticDictionary {
    semantic_descriptors: DashSet<SemanticDescriptor, ahash::RandomState>,
    shared_words: DashSet<SharedWord, ahash::RandomState>,
}
impl Display for SemanticDictionary {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.semantic_descriptors
            .iter()
            .try_for_each(|descriptor| {
                write!(f, "{}", *descriptor)
            })
    }
}

#[derive(Debug)]
pub struct FromStrError;
impl Display for FromStrError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl StdError for FromStrError {}
impl FromStr for SemanticDictionary {
    type Err = FromStrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.lines().try_fold(SemanticDictionary::new(), |acc, x| {
            acc.semantic_descriptors
                .insert(SemanticDescriptor::from_str(x, &acc.shared_words)?);
            Ok(acc)
        })
    }
}

lazy_static::lazy_static!(
    static ref STOP_WORDS: HashSet<&'static str, ahash::RandomState> = {
        HashSet::from_iter([
            "a",
            "able",
            "about",
            "above",
            "abst",
            "accordance",
            "according",
            "accordingly",
            "across",
            "act",
            "actually",
            "added",
            "adj",
            "affected",
            "affecting",
            "affects",
            "after",
            "afterwards",
            "again",
            "against",
            "ah",
            "all",
            "almost",
            "alone",
            "along",
            "already",
            "also",
            "although",
            "always",
            "am",
            "among",
            "amongst",
            "an",
            "and",
            "announce",
            "another",
            "any",
            "anybody",
            "anyhow",
            "anymore",
            "anyone",
            "anything",
            "anyway",
            "anyways",
            "anywhere",
            "apparently",
            "approximately",
            "are",
            "aren",
            "arent",
            "arise",
            "around",
            "as",
            "aside",
            "ask",
            "asking",
            "at",
            "auth",
            "available",
            "away",
            "awfully",
            "b",
            "back",
            "be",
            "became",
            "because",
            "become",
            "becomes",
            "becoming",
            "been",
            "before",
            "beforehand",
            "begin",
            "beginning",
            "beginnings",
            "begins",
            "behind",
            "being",
            "believe",
            "below",
            "beside",
            "besides",
            "between",
            "beyond",
            "biol",
            "both",
            "brief",
            "briefly",
            "but",
            "by",
            "c",
            "ca",
            "came",
            "can",
            "cannot",
            "can't",
            "cause",
            "causes",
            "certain",
            "certainly",
            "co",
            "com",
            "come",
            "comes",
            "contain",
            "containing",
            "contains",
            "could",
            "couldnt",
            "d",
            "date",
            "did",
            "didn't",
            "different",
            "do",
            "does",
            "doesn't",
            "doing",
            "done",
            "don't",
            "down",
            "downwards",
            "due",
            "during",
            "e",
            "each",
            "ed",
            "edu",
            "effect",
            "eg",
            "eight",
            "eighty",
            "either",
            "else",
            "elsewhere",
            "end",
            "ending",
            "enough",
            "especially",
            "et",
            "et-al",
            "etc",
            "even",
            "ever",
            "every",
            "everybody",
            "everyone",
            "everything",
            "everywhere",
            "ex",
            "except",
            "f",
            "far",
            "few",
            "ff",
            "fifth",
            "first",
            "five",
            "fix",
            "followed",
            "following",
            "follows",
            "for",
            "former",
            "formerly",
            "forth",
            "found",
            "four",
            "from",
            "further",
            "furthermore",
            "g",
            "gave",
            "get",
            "gets",
            "getting",
            "give",
            "given",
            "gives",
            "giving",
            "go",
            "goes",
            "gone",
            "got",
            "gotten",
            "h",
            "had",
            "happens",
            "hardly",
            "has",
            "hasn't",
            "have",
            "haven't",
            "having",
            "he",
            "hed",
            "hence",
            "her",
            "here",
            "hereafter",
            "hereby",
            "herein",
            "heres",
            "hereupon",
            "hers",
            "herself",
            "hes",
            "hi",
            "hid",
            "him",
            "himself",
            "his",
            "hither",
            "home",
            "how",
            "howbeit",
            "however",
            "hundred",
            "i",
            "id",
            "ie",
            "if",
            "i'll",
            "im",
            "immediate",
            "immediately",
            "importance",
            "important",
            "in",
            "inc",
            "indeed",
            "index",
            "information",
            "instead",
            "into",
            "invention",
            "inward",
            "is",
            "isn't",
            "it",
            "itd",
            "it'll",
            "its",
            "itself",
            "i've",
            "j",
            "just",
            "k",
            "keep",
            "keeps",
            "kept",
            "kg",
            "km",
            "know",
            "known",
            "knows",
            "l",
            "largely",
            "last",
            "lately",
            "later",
            "latter",
            "latterly",
            "least",
            "less",
            "lest",
            "let",
            "lets",
            "like",
            "liked",
            "likely",
            "line",
            "little",
            "'ll",
            "look",
            "looking",
            "looks",
            "ltd",
            "m",
            "made",
            "mainly",
            "make",
            "makes",
            "many",
            "may",
            "maybe",
            "me",
            "mean",
            "means",
            "meantime",
            "meanwhile",
            "merely",
            "mg",
            "might",
            "million",
            "miss",
            "ml",
            "more",
            "moreover",
            "most",
            "mostly",
            "mr",
            "mrs",
            "much",
            "mug",
            "must",
            "my",
            "myself",
            "n",
            "na",
            "name",
            "namely",
            "nay",
            "nd",
            "near",
            "nearly",
            "necessarily",
            "necessary",
            "need",
            "needs",
            "neither",
            "never",
            "nevertheless",
            "new",
            "next",
            "nine",
            "ninety",
            "no",
            "nobody",
            "non",
            "none",
            "nonetheless",
            "noone",
            "nor",
            "normally",
            "nos",
            "not",
            "noted",
            "nothing",
            "now",
            "nowhere",
            "o",
            "obtain",
            "obtained",
            "obviously",
            "of",
            "off",
            "often",
            "oh",
            "ok",
            "okay",
            "old",
            "omitted",
            "on",
            "once",
            "one",
            "ones",
            "only",
            "onto",
            "or",
            "ord",
            "other",
            "others",
            "otherwise",
            "ought",
            "our",
            "ours",
            "ourselves",
            "out",
            "outside",
            "over",
            "overall",
            "owing",
            "own",
            "p",
            "page",
            "pages",
            "part",
            "particular",
            "particularly",
            "past",
            "per",
            "perhaps",
            "placed",
            "please",
            "plus",
            "poorly",
            "possible",
            "possibly",
            "potentially",
            "pp",
            "predominantly",
            "present",
            "previously",
            "primarily",
            "probably",
            "promptly",
            "proud",
            "provides",
            "put",
            "q",
            "que",
            "quickly",
            "quite",
            "qv",
            "r",
            "ran",
            "rather",
            "rd",
            "re",
            "readily",
            "really",
            "recent",
            "recently",
            "ref",
            "refs",
            "regarding",
            "regardless",
            "regards",
            "related",
            "relatively",
            "research",
            "respectively",
            "resulted",
            "resulting",
            "results",
            "right",
            "run",
            "s",
            "said",
            "same",
            "saw",
            "say",
            "saying",
            "says",
            "sec",
            "section",
            "see",
            "seeing",
            "seem",
            "seemed",
            "seeming",
            "seems",
            "seen",
            "self",
            "selves",
            "sent",
            "seven",
            "several",
            "shall",
            "she",
            "shed",
            "she'll",
            "shes",
            "should",
            "shouldn't",
            "show",
            "showed",
            "shown",
            "showns",
            "shows",
            "significant",
            "significantly",
            "similar",
            "similarly",
            "since",
            "six",
            "slightly",
            "so",
            "some",
            "somebody",
            "somehow",
            "someone",
            "somethan",
            "something",
            "sometime",
            "sometimes",
            "somewhat",
            "somewhere",
            "soon",
            "sorry",
            "specifically",
            "specified",
            "specify",
            "specifying",
            "still",
            "stop",
            "strongly",
            "sub",
            "substantially",
            "successfully",
            "such",
            "sufficiently",
            "suggest",
            "sup",
            "sure",
            "t",
            "take",
            "taken",
            "taking",
            "tell",
            "tends",
            "th",
            "than",
            "thank",
            "thanks",
            "thanx",
            "that",
            "that'll",
            "thats",
            "that've",
            "the",
            "their",
            "theirs",
            "them",
            "themselves",
            "then",
            "thence",
            "there",
            "thereafter",
            "thereby",
            "thered",
            "therefore",
            "therein",
            "there'll",
            "thereof",
            "therere",
            "theres",
            "thereto",
            "thereupon",
            "there've",
            "these",
            "they",
            "theyd",
            "they'll",
            "theyre",
            "they've",
            "think",
            "this",
            "those",
            "thou",
            "though",
            "thoughh",
            "thousand",
            "throug",
            "through",
            "throughout",
            "thru",
            "thus",
            "til",
            "tip",
            "to",
            "together",
            "too",
            "took",
            "toward",
            "towards",
            "tried",
            "tries",
            "truly",
            "try",
            "trying",
            "ts",
            "twice",
            "two",
            "u",
            "un",
            "under",
            "unfortunately",
            "unless",
            "unlike",
            "unlikely",
            "until",
            "unto",
            "up",
            "upon",
            "ups",
            "us",
            "use",
            "used",
            "useful",
            "usefully",
            "usefulness",
            "uses",
            "using",
            "usually",
            "v",
            "value",
            "various",
            "'ve",
            "very",
            "via",
            "viz",
            "vol",
            "vols",
            "vs",
            "w",
            "want",
            "wants",
            "was",
            "wasnt",
            "way",
            "we",
            "wed",
            "welcome",
            "we'll",
            "went",
            "were",
            "werent",
            "we've",
            "what",
            "whatever",
            "what'll",
            "whats",
            "when",
            "whence",
            "whenever",
            "where",
            "whereafter",
            "whereas",
            "whereby",
            "wherein",
            "wheres",
            "whereupon",
            "wherever",
            "whether",
            "which",
            "while",
            "whim",
            "whither",
            "who",
            "whod",
            "whoever",
            "whole",
            "who'll",
            "whom",
            "whomever",
            "whos",
            "whose",
            "why",
            "widely",
            "willing",
            "wish",
            "with",
            "within",
            "without",
            "wont",
            "words",
            "world",
            "would",
            "wouldnt",
            "www",
            "x",
            "y",
            "yes",
            "yet",
            "you",
            "youd",
            "you'll",
            "your",
            "youre",
            "yours",
            "yourself",
            "yourselves",
            "you've",
            "z",
        ])
    };
);

pub struct Sentence<'a> {
    data: &'a str,
}

impl<'a, 'b> Sentence<'a> {
    pub fn new(data: &'a str) -> Self {
        Self { data }
    }
}
impl<'a> Iterator for Sentence<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        let start = self.data.find(|c: char| !c.is_whitespace())?;
        self.data = &self.data[start..];
        self.data
            .find(char::is_whitespace)
            .map(|idx| {
                let get = &self.data[..idx];
                self.data = &self.data[idx + 1..];
                get.trim_start_matches(&['"', '\''][..])
                    .trim_end_matches(&['"', '\'', ',', ';', ':'][..])
            })
            .and_then(|s| {
                if s.is_empty()
                    || STOP_WORDS.contains(s)
                    || s.contains(|c: char| !c.is_alphabetic() && !matches!(c, '\'' | '-'))
                {
                    return self.next();
                }
                Some(s)
            })
    }
}
pub struct SentenceList<'a> {
    data: &'a str,
}

impl<'a> SentenceList<'a> {
    pub fn new(data: &'a str) -> Self {
        Self { data }
    }
}
impl<'a> Iterator for SentenceList<'a> {
    type Item = Sentence<'a>;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        const SENTENCE_END: &[char] = &['.', '?', '!'];
        self.data.find(SENTENCE_END).map(|idx| {
            let get = &self.data[..idx];
            self.data = &self.data[idx + 1..];
            Sentence::new(get)
        })
    }
}
pub struct ZeroCopySentenceLists<'a> {
    iter: SplitWhitespace<'a>,
}
impl<'a> Iterator for ZeroCopySentenceLists<'a> {
    type Item = Vec<&'a str>;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        const SENTENCE_END: &[char] = &['.', '?', '!'];
        let Self {
            ref mut iter,
        } = *self;
        let mut out = Vec::new();
        loop {
            let next = iter
                .map(|s| {
                    s.trim_start_matches(&['"', '\''][..])
                        .trim_end_matches(&['"', '\'', ',', ';', ':'][..])
                })
                .filter(|s| {
                    s.trim_end_matches(SENTENCE_END)
                        .chars()
                        .all(|c| c.is_alphabetic() || matches!(c, '\'' | '-'))
                })
                .filter(|s| !s.is_empty())
                .find(|s| !STOP_WORDS.contains(s))?;

            if let Some(s) = next.strip_suffix(SENTENCE_END) {
                out.push(s);
                break Some(out);
            } else {
                out.push(next)
            }
        }
    }
}
impl<'a> ZeroCopySentenceLists<'a> {
    pub fn new(buf: &'a str) -> Self {
        Self {
            iter: buf.split_whitespace(),
        }
    }
}
/* pub struct SentenceLists<'a> {
    buf: Chars<'a>,
    stop_words: HashSet<&'static str, ahash::RandomState>,
}

impl<'a> SentenceLists<'a> {
    pub fn new(buf: &'a str) -> Self {
        Self {
            buf: buf.chars(),
            stop_words: {
                HashSet::from_iter([
                    "i", "a", "about", "an", "are", "as", "at", "be", "by", "for", "from", "how",
                    "in", "is", "it", "of", "on", "or", "that", "the", "this", "to", "was", "what",
                    "when", "where", "who", "will", "with",
                ])
            },
        }
    }
}
impl<'a> Iterator for SentenceLists<'a> {
    type Item = Vec<String>;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let mut word = String::new();
        let mut sentence_list = Vec::new();
        let Self {
            ref mut buf,
            ref stop_words,
        } = *self;
        loop {
            let c = buf.next()?;
            if c.is_alphabetic() {
                word.push(c);
            } else {
                if !word.is_empty() {
                    if stop_words.contains(&*word) {
                        word.clear();
                        continue;
                    }
                    sentence_list.push(mem::take(&mut word));
                }
                if matches!(c, '?' | '.' | '!') && !sentence_list.is_empty() {
                    break Some(sentence_list);
                }
            }
        }
    }
} */
use std::error::Error as StdError;
impl SemanticDictionary {
    pub fn read_from_file(file: &mut impl Read) -> Result<Self, Box<dyn StdError>> {
        let file = BufReader::with_capacity(1024 * 1024 * 8, file);
        file.lines().try_fold(SemanticDictionary::new(), |acc, x| {
            let x = x?;
            acc.semantic_descriptors
                .insert(SemanticDescriptor::from_str(&x, &acc.shared_words)?);
            Ok(acc)
        })
    }
    pub fn write_to_file(&self, file: &mut impl Write) -> io::Result<()> {
        let mut file = BufWriter::with_capacity(1024 * 1024 * 8, file);
        self.semantic_descriptors
        .iter()
        .try_for_each(|descriptor| {
            writeln!(file, "{}", *descriptor)
        })
    }
    pub fn do_solve(&self) -> io::Result<()> {
        println!("Enter a TOEFL question as <word> <answer> <choice1> <choice2> <choice3>");
        let mut read = String::new();
        io::stdin().read_line(&mut read)?;
        let mut iter = read.split_whitespace();
        let (word, answer, choice1, choice2, choice3) = iter
            .next()
            .and_then(|word| Some((word, iter.next()?, iter.next()?, iter.next()?, iter.next()?)))
            .expect("Failed to parse TOEFL question");
        let choices = &[choice1, choice2, choice3];
        let most_similar = self
            .most_similar_word(word, choices)
            .expect("Most similar word not found");
        let similarity = self.get_similarity(word, most_similar);

        println!(
            concat!("Most similar: {}\n", "Index: {:.6}\n", "{}orrect answer"),
            most_similar,
            similarity,
            if most_similar == answer { "C" } else { "Inc" }
        );

        Ok(())
    }
    pub fn do_write(&self) -> Result<(), Box<dyn StdError>> {
        let mut db = File::create(DB_PATH)?;
        self.write_to_file(&mut db)?;
        Ok(())
    }
    pub fn do_read(&mut self) -> Result<(), Box<dyn StdError>> {
        let mut file = File::open("SemanticDictionary.txt")?;
        let new = SemanticDictionary::read_from_file(&mut file)?;
        *self = new;

        Ok(())
    }
    pub fn do_read_process(&self) -> io::Result<()> {
        print!("Enter file name(s) (or * for all txt files in the current directory): ");
        io::stdout().flush()?;
        let mut input = String::new();
        io::stdin().read_line(&mut input)?;
        let path = input.trim();

        let time = Instant::now();

        let paths: Vec<Cow<Path>> = if path == "*" {
            print!("How many files [512]?: ");
            let mut count = String::new();
            io::stdout().flush()?;
            io::stdin().read_line(&mut count)?;
            let count = count.trim().parse::<usize>().unwrap_or(512);

            fs::read_dir("./")?
                .map(|entry| entry.map(|i| i.path()))
                .filter(|entry| entry.as_ref().map_or(true, |f| f.is_file()))
                .take(count)
                .map(|i| i.map(Cow::Owned))
                .collect::<Result<Vec<_>, _>>()?
        } else {
            path.split_whitespace().map(Path::new).map(Cow::Borrowed).collect()
        };

        let contents = 
                paths
                .into_par_iter()
                .filter(|i| i.file_name().map_or(false, |i| i != DB_PATH))
                .filter(|i| i.extension().map_or(false, |f| f == "txt"))
                .map(File::open)
                .map(|i| {
                    let mut out = Vec::new();
                    let mut i = i?;
                    i.read_to_end(&mut out)?;

                    io::Result::Ok(String::from_utf8_lossy(&out).to_lowercase())
                })
                .collect::<Result<Vec<String>, _>>()?;
        let sentences: Vec<Vec<SharedWord>> = contents
            .iter()
            .flat_map(|i| SentenceList::new(i))
            .par_bridge()
            .map(|sentence| {
                sentence
                    .map(|i| {
                        self.shared_words.get(i).map_or_else(|| {
                            let get = SharedWord::new(Arc::new(i.to_owned()));
                            self.shared_words.insert(get.clone());
                            get
                        }, |value| value.clone())
                    })
                    .collect()
            })
            .collect();

        drop(contents);

        sentences.into_par_iter().for_each(|sentence| {
            for target_word in &sentence {
                self.process_target_word(target_word, &sentence)
            }
        });
        eprintln!("Done! ({:?})", time.elapsed());
        Ok(())
    }
    /* pub fn from_buf_read<T>(reader: T) -> Result<Self, Box<dyn StdError>>
    where
        T: BufRead
    {
        reader
        .lines()
        .try_fold(SemanticDictionary::new(), |acc, x| {
            acc.semantic_descriptors
                .insert(SemanticDescriptor::from_str(&x?)?);
            Ok(acc)
        })
    } */
    pub fn new() -> Self {
        Self {
            shared_words: DashSet::default(),
            semantic_descriptors: DashSet::default(),
        }
    }
    pub fn get_similarity(&self, first: &str, second: &str) -> f64 {
        let a = &*self.semantic_descriptors.get(first).unwrap();
        let b = &*self.semantic_descriptors.get(second).unwrap();
        (a * b) as f64 / (((a * a) as f64).sqrt() * ((b * b) as f64).sqrt())
    }
    pub fn most_similar_word<'a>(&self, word: &str, choices: &'a [&str]) -> Option<&'a str> {
        choices
            .iter()
            .map(|choice| (choice, self.get_similarity(word, choice)))
            .max_by(|(_, similarity_lhs), (_, similarity_rhs)| {
                similarity_lhs
                    .partial_cmp(similarity_rhs)
                    .unwrap_or(std::cmp::Ordering::Equal)
            })
            .map(|(choice, _)| &**choice)
    }
    pub fn process_target_word(&self, target_word: &SharedWord, sentence_list: &[SharedWord]) {
        if let Some(target_word) = self.semantic_descriptors.get(&****target_word) {
            sentence_list
                .iter()
                .filter(|i| target_word.get_target_word() != ****i)
                .for_each(|i| target_word.process_context_word(i))
        } else {
            let new = SemanticDescriptor::new(target_word.clone());
            sentence_list
                .iter()
                .filter(|i| new.get_target_word() != ****i)
                .for_each(|i| new.process_context_word(i));
            self.semantic_descriptors.insert(new);
        }
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct SharedWord(Arc<String>);

impl std::ops::Deref for SharedWord {
    type Target = Arc<String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl SharedWord {
    pub const fn new(item: Arc<String>) -> Self {
        Self(item)
    }
}
impl Borrow<str> for SharedWord {
    fn borrow(&self) -> &str {
        &self.0
    }
}
