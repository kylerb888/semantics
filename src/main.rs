use crate::semantic_dictionary::SemanticDictionary;
use std::{io, mem, str::FromStr};

mod semantic_descriptor;
mod semantic_dictionary;
#[derive(Debug)]
struct UserChoiceInvalid;
#[derive(PartialEq)]
enum UserChoice {
    Read,
    Write,
    ReadProcess,
    Solve,
    Quit,
}
const DB_PATH: &str = "SemanticDictionary.txt";
use std::error::Error as StdError;
impl UserChoice {
    pub fn execute(&self, dict: &mut SemanticDictionary) -> Result<(), Box<dyn StdError>> {
        use UserChoice::*;
        match self {
            ReadProcess => dict.do_read_process()?,
            Write => dict.do_write()?,
            Read => dict.do_read()?,
            Solve => dict.do_solve()?,
            _ => todo!(),
        }
        Ok(())
    }
}
impl FromStr for UserChoice {
    type Err = UserChoiceInvalid;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use UserChoice::*;
        let out = match s.trim() {
            "1" => Read,
            "2" => Write,
            "3" => ReadProcess,
            "4" => Solve,
            "5" => Quit,
            _ => return Err(UserChoiceInvalid),
        };
        Ok(out)
    }
}
fn print_menu() {
    println!(concat!(
        "1.  Read the semantic dictionary from storage\n",
        "2.  Write the semantic dictionary to storage\n",
        "3.  Read and process a text file (get smarter!)\n",
        "4.  Solve a TOEFL question\n",
        "5.  Quit\n"
    ))
}
fn main() {
    let stdin = io::stdin();
    let mut input = String::new();
    let mut dict = SemanticDictionary::new();
    loop {  
        print_menu();
        stdin
            .read_line(&mut input)
            .expect("Failed to read line from stdout");
        let choice = UserChoice::from_str(&input).expect("Failed to parse input");
        if choice == UserChoice::Quit {
            break;
        }

        choice.execute(&mut dict).expect("Failed to execute");

        input.clear();
    }
}
